FROM debian:latest

# Install pygments (for syntax highlighting) 
RUN apt-get -qq update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends libstdc++6 python3-pygments git ca-certificates asciidoc asciidoctor curl docutils-common python3-docutils rsync openssh-client rubygems ruby-dev build-essential \
	&& rm -rf /var/lib/apt/lists/*

# Install ascidoc plugins
RUN gem install asciidoctor-bibtex asciidoctor-html5s asciidoctor-lazy-images

# Configuration variables
ENV HUGO_VERSION v0.136.3
ENV HUGO_BINARY hugo_extended_${HUGO_VERSION}_linux-amd64.tar.gz
ENV SITE_DIR '/usr/share/blog'
ENV PAGEFIND_VERSION v1.1.1

# Download and install pagefind
RUN curl -sL -o /tmp/pagefind.tar.gz https://github.com/CloudCannon/pagefind/releases/download/${PAGEFIND_VERSION}/pagefind-${PAGEFIND_VERSION}-x86_64-unknown-linux-musl.tar.gz && \
    tar -xf /tmp/pagefind.tar.gz -C /bin/ && \
    chmod +x /bin/pagefind
    
# Download and install hugo
RUN curl -sL -o /tmp/hugo.tar.gz \
    https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} && \
    tar -xf /tmp/hugo.tar.gz -C /bin/ && \
    chmod +x /bin/hugo && \
    rm /tmp/hugo.tar.gz && \
    mkdir ${SITE_DIR}

WORKDIR ${SITE_DIR}
